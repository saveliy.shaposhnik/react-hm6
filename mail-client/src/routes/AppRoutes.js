import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";
import Cart from '../pages/Cart/Cart';
import Main from '../pages/Main/Main';
import Favourites from "../pages/Favourite/Favourite"


export default function AppRoutes(props) {
    const {setItems,setLiked,openModal} = props;
    return (
        <div className="app-routes">
            <Switch>
                <Redirect exact from="/" to="/main" />
                <Route exact path="/main" render={() => <Main setItems={setItems} setLiked={setLiked} onClick={openModal}/>} />
                <Route exact path="/cart" render={() => <Cart setItems={setItems} setLiked={setLiked} onClick={openModal}/>} />
                <Route exact path="/favourites" render={() => <Favourites setItems={setItems} setLiked={setLiked} onClick={openModal}/>} />
                <Route path="*" render={() => <div>404</div>} />
            </Switch>
        </div>
    )
}
