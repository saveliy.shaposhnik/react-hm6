import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import Items from '../../components/Items/Items';
import "./Main.scss"

export default function Main(props) {
    
    const { onClick, setLiked} = props;
    const [isMainOpen] = useState(true)
    const items = useSelector(state => state.items.items.data);

    return (
        <div className="cards-div">
            {items.map(el => {
                return <Items key={el.id} items={el} setLiked={setLiked} onClick={onClick} isMainOpen={isMainOpen}/>
            })}
        </div>
    )
}
