import { TOGGLE_MODAL } from "./types"

const initialStore = {
    modal:{
        data:{
            title:"",
            parag:"",
        },
        isActive:false,
    }
}


const reducer = (state = initialStore,action) => {
    switch(action.type){
        case TOGGLE_MODAL:
            return {modal:{...state.modal,isActive:action.payload}}
        default:
            return state
    }
}

export default reducer;

