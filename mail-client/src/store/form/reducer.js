import { TOGGLE_FORM } from "./types"

const initialStore = {
    form:{
        isActive:false,
    }
}


const reducer = (state = initialStore,action) => {
    switch(action.type){
        case TOGGLE_FORM:
            return ({...state,form:{...state.form,isActive:action.payload}})
        default:
            return state
    }
}

export default reducer;