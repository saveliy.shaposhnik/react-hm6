export const LOAD_ITEMS_REQUEST = "LOAD_ITEMS_REQUEST";
export const LOAD_ITEMS_SUCCESS = "LOAD_ITEMS_SUCCESS";
export const SET_ITEMS = "SET_ITEMS";
export const SET_ITEM = "SET_ITEM";