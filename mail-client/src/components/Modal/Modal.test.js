import { render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import Modal from './Modal'

describe("Testing Modal.js",() => {
    test('Smoke test Modal.js', () => {
        render(<Modal/>)
    })

    test("Testing props text on Modal.js",() => {
        const header = "header";
        const text = "title";

        const { getByTestId } = render(<Modal header={header} text={text}/>)

        
        expect(getByTestId("header").textContent).toBe(header)
        expect(getByTestId("text").textContent).toBe(text)
    })
    
    test('Testing functions on Modal.js', () => {
        const header = "header";
        const text = "title";
        const closeModal = jest.fn()
        const addToCart = jest.fn()

        const { getByText } = render(<Modal header={header} text={text} closeModal={closeModal} addToCart={addToCart}/>)

        expect(closeModal).not.toHaveBeenCalled()
        userEvent.click(getByText('Cancel'))
        expect(closeModal).toHaveBeenCalled()

        expect(addToCart).not.toHaveBeenCalled()
        userEvent.click(getByText('Ok'))
        expect(addToCart).toHaveBeenCalled()
    })

    test("Testing that Modal.js contains close button when closeBtn equal true",() => {
        const addToCart = jest.fn()

        render(<Modal closeButton/>)

        const closeButton = document.getElementsByClassName('closeModalBtn')[0];
        
        expect(closeButton).toBeInTheDocument()
    })
    
})