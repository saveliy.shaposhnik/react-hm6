import "./Button.scss";
import React from 'react'

export default function Button(props) {
    const { text, backgroundColor, onClick, className,disabled } = props; 

    return (
        <>
            <button disabled={disabled} style={{ backgroundColor: backgroundColor }} className={className} onClick={onClick} data-testid="button">{text}</button>
        </>
    )
}


