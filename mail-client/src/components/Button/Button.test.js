import { render } from '@testing-library/react'
import Button from './Button'
import userEvent from '@testing-library/user-event'

describe("Testing Button.js", () => {
    test("Smoke test Button.js", () => {
        render(<Button />)
    })

    test("Button.js should contain text", () => {
        const text = "text"

        const { getByTestId } = render(<Button text={text} />)

        const buttonText = getByTestId("button").textContent

        expect(buttonText).toBe(text)
    })

    test('Testing that function will be called', () => {
        const text = "text"
        const onClick = jest.fn()

        const { getByTestId } = render(<Button text={text} onClick={onClick} />)

        const buttonText = getByTestId("button")

        expect(onClick).not.toHaveBeenCalled()

        userEvent.click(buttonText)

        expect(onClick).toHaveBeenCalled()
    })

})